#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <SFML/Graphics.hpp>
#include <thread>
#include <string>
#define PORT 44493
#define MAXPLAYERS 3


std::vector<std::thread> players;
std::vector<std::thread> msgToClient;
std::vector<int> socks;
std::vector<std::pair<int,int>> coords(2,{400,400});

int clientNumber = 1;


void anyClient(int clientSock,int number){
    bool flag = true ;
    while(clientNumber < (MAXPLAYERS+2)) {
        if (clientNumber != (MAXPLAYERS + 1)){
            std::string protocol = "!@#" + std::to_string(number - 1);
            const char *message = protocol.c_str() ;
            send(clientSock, message, sizeof(message) / sizeof(char), 0);
            usleep(5000000);
        }
        else if (flag){
            std::string protocol = "!@#" + std::to_string(number - 1);
            const char *message = protocol.c_str() ;
            send(clientSock, message, sizeof(message) / sizeof(char), 0);
            flag = false ;
        }
        else{
            std::pair <int,int> a;
            char receive_char[8];
            int resultReceive = recv(clientSock, receive_char, 8, 0);
            int space;
            while (resultReceive > 0) {

                std::string tmp;
                resultReceive = recv(clientSock, receive_char, 8, 0);
                tmp = receive_char;

                if(tmp[0] == 'd') {
                    for (auto client : socks) {
                        std::string strichka;
                        strichka += tmp;
                        strichka += "#";
                        const char *message = strichka.c_str();
                        send(client, message, 32, 0);
                    }
                } else {
                    space = tmp.find(' ');
                    a = {std::stoi(tmp.substr(0, space)), std::stoi(tmp.substr(space + 1, tmp.size() - 1))};
                    coords[number - 1] = a;

                    for (auto client : socks) {
                        std::string strichka;
                        strichka += std::to_string(coords[0].first) + " " + std::to_string(coords[0].second) + "#";
                        strichka += std::to_string(coords[1].first) + " " + std::to_string(coords[1].second) + "#";
                        strichka += std::to_string(coords[2].first) + " " + std::to_string(coords[2].second) + "#";
                        const char *message = strichka.c_str();
                        send(client, message, 32, 0);
                    }
                }
            }
            for (auto &msg : msgToClient) {
                msg.join();
            }
            for (auto client : socks) {
                close(client);
            }
        }
    }
}

int main() {

    int sockfd;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);


    if (sockfd < 0) {
        auto error("ERROR opening socket");
    }


    sockaddr_in server;
    server.sin_addr.s_addr = 0;
    server.sin_family = AF_INET;
    server.sin_port = htons(PORT);

    if (bind(sockfd, (struct sockaddr *) &server, sizeof(server)) < 0) {
        std::cout << "bind errror" << std::endl;
        return 1;
    }

    if (listen(sockfd, 3) < 0) {
        std::cout << "listen errror" << std::endl;
        return 1;
    }

    for(int i = 0; i < 3 ; ++i) {
        int clientSocket;
        sockaddr_in clientAdress;
        socklen_t sizeCA = sizeof(clientAdress);
        clientSocket = accept(sockfd, (struct sockaddr *) &clientAdress, &sizeCA);

        socks.push_back(clientSocket);

        int playerCounter = 0;
        for(auto client : socks){
            playerCounter++;
        }
        for (auto client : socks) {

            std::string strichka;
            strichka += "new " + std::to_string(playerCounter);
            const char * message = strichka.c_str();

            send(client, message,19, 0);
        }

        std::cout<<"client is activated"<<std::endl;

        if (clientSocket < 0) {
            std::cout << "accept failed" << std::endl;
        } else {
            std::thread client(anyClient, clientSocket, clientNumber);
            clientNumber++;
            players.push_back(std::move(client));
        }
    }
    for (auto & player : players) {
        player.join();
    }
    return 0;
}